core = 7.x
api = 2



; Modules - Contrib
projects[admin_menu][subdir] = 'contrib'
projects[admin_menu][version] = '3.0-rc1'

projects[answers][subdir] = 'contrib'
projects[answers][version] = '3.0'

projects[aup][subdir] = 'contrib'
projects[aup][version] = '1.0-beta1'

projects[backup_migrate][subdir] = 'contrib'
projects[backup_migrate][version] = '2.2'

projects[bakery][subdir] = 'contrib'
projects[bakery][type] = 'module'
projects[bakery][download][type] = 'git'
projects[bakery][download][url] = 'http://git.drupal.org/project/bakery.git'
projects[bakery][download][revision] = 'bd7d6d1dd8dfab391435a4ad293febf43945c6a3'

projects[best_answer][subdir] = 'contrib'
projects[best_answer][version] = '3.0'

projects[coder][subdir] = 'contrib'
projects[coder][version] = '1.0'

projects[commentcloser][subdir] = 'contrib'
projects[commentcloser][type] = 'module'
projects[commentcloser][download][type] = 'git'
projects[commentcloser][download][url] = 'http://git.drupal.org/project/commentcloser.git'
projects[commentcloser][download][revision] = '82bc4a44a8fb8b729c9d7ebefd7a231212b2cddd'

projects[context][subdir] = 'contrib'
projects[context][version] = '3.0-beta2'

projects[context_admin][subdir] = 'contrib'
projects[context_admin][version] = '1.1'

projects[ctools][subdir] = 'contrib'
projects[ctools][version] = '1.0-rc1'

projects[devel][subdir] = 'contrib'
projects[devel][version] = '1.2'

projects[delta][subdir] = 'contrib'
projects[delta][version] = '3.0-beta9'

projects[diff][subdir] = 'contrib'
projects[diff][version] = '2.0'

projects[ds][subdir] = 'contrib'
projects[ds][version] = '1.5'

projects[features][subdir] = 'contrib'
projects[features][version] = '1.0-beta6'

projects[flag][subdir] = 'contrib'
projects[flag][version] = '2.0-beta6'

projects[flag_abuse][subdir] = 'contrib'
projects[flag_abuse][type] = 'module'
projects[flag_abuse][download][type] = 'git'
projects[flag_abuse][download][url] = 'http://git.drupal.org/project/flag_abuse.git'
projects[flag_abuse][download][revision] = '6545d190d1f605dfdaa69e664e69f87849b57861'

projects[less][subdir] = 'contrib'
projects[less][version] = '2.4'

projects[libraries][subdir] = 'contrib'
projects[libraries][version] = '2.0-alpha2'

projects[module_filter][subdir] = 'contrib'
projects[module_filter][version] = '1.6'

projects[notifications][subdir] = 'contrib'
projects[notifications][version] = '1.0-alpha2'

projects[omega_tools][subdir] = 'contrib'
projects[omega_tools][version] = '3.0-rc4'

projects[panels][subdir] = 'contrib'
projects[panels][version] = '3.0'

projects[pathauto][subdir] = 'contrib'
projects[pathauto][version] = '1.0'

projects[radioactivity][subdir] = 'contrib'
projects[radioactivity][version] = '2.4'

projects[relevant_answers][subdir] = 'contrib'
projects[relevant_answers][version] = '3.0'

projects[rules][subdir] = 'contrib'
projects[rules][version] = '2.1'

projects[token][subdir] = 'contrib'
projects[token][version] = '1.0-rc1'

projects[userpoints][subdir] = 'contrib'
projects[userpoints][version] = '1.0'

projects[views][subdir] = 'contrib'
projects[views][version] = '3.1'

projects[vote_up_down][subdir] = 'contrib'
projects[vote_up_down][type] = 'module'
projects[vote_up_down][download][type] = 'git'
projects[vote_up_down][download][url] = 'http://git.drupal.org/project/vote_up_down.git'
projects[vote_up_down][download][revision] = '1bdfa11fae26835cc7f7541b87282ea498ae7694'

projects[votingapi][subdir] = 'contrib'
projects[votingapi][version] = '2.6'


; Themes - Contrib
projects[omega][subdir] = 'contrib'
projects[omega][version] = '3.1'

projects[rubik][subdir] = 'contrib'
projects[rubik][version] = '4.0-beta7'

projects[tao][subdir] = 'contrib'
projects[tao][version] = '3.0-beta4'



; libraries
